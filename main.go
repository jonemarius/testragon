
package main

import (
  "github.com/aws/aws-lambda-go/events"
  "github.com/aws/aws-lambda-go/lambda"
  "github.com/epsagon/epsagon-go/epsagon"
  "log"
)

func myHandler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
  log.Println("In myHandler, received body: ", request.Body)
  return events.APIGatewayProxyResponse{Body: request.Body, StatusCode: 200}, nil
}

func main() {
  log.Println("enter main")
  lambda.Start(epsagon.WrapLambdaHandler(
    &epsagon.Config{ApplicationName: "testragon", Token: "b0d8c3b7-b590-4047-9175-4c1d26c96e58"},
    myHandler))
}
